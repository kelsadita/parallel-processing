#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct arc{
    int to;
    int from;
    int capacity;
    int flow;
}ARC;

typedef struct vertex{
    int label;
    int value;
    int excess;
}VERTEX;



