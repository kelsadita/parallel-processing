#include<stdio.h>
#include<stdlib.h>
static int st = 31;
/*
 * randint() function is called to generate random capacities for different arcs
 */

int randint(int max)  
{ 
	int min = 1;
	srand(time(NULL) - st );
	st *= 313;
	return min+(int)((int)(max-min+1)*(rand()/(RAND_MAX+1.0))); 
}  

int main(int argc, char* argv[] )
{
    /*
     * Checking whether the user has passed correct number of arguments
     */
	if(argc != 9){
		printf(" Format Error : exit(0)\n Usage: ./gen -v <vertices> -c <capacity> -s <start-vertex> -f <filename> \n ");
		exit(0);
	}

	FILE *f = fopen(argv[8],"w");
	int i,j;
	int v = atoi(argv[2]);
    /*
     * Calculating the number of edges using simple formula
     * Basically we need the upper trianglular matrix of the matrix V*V
     */
	int e = (v * (v - 1)) / 2;
	int c = atoi(argv[4]);
	int s = atoi(argv[6]);
    /*
     *First printing the vertices and edges in the required file
     */
	fprintf(f,"%d\t%d\n",v,e);
    /*
     *Count variable to count the total number of arcs
     */
	int count = 0;
    /*
     * Checking if the starting vertex is 1 or 0
     * for desired arcs
     */
	if( s == 1 ){
		for( i = 1 ; i < v ; i++ ){
			for ( j = i + 1; j <= v ; j++ ){
                fprintf(f,"%d\t%d\t%d\n",i,j,randint(c));
                count++;
			}
		}
	} else {
		for( i = 0 ; i < v-1 ; i++ ){
			for ( j = i + 1 ; j <= v - 1 ; j++ ){
                fprintf(f,"%d\t%d\t%d\n",i,j,randint(c));
                count++;
			}
		}
	}
	printf("\n File created : %s \t[ OK ]\n ARC COUNT : %d \t[ OK ]\n\n",argv[8],count);
	fclose(f);
	return 0;
}


