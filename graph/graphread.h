#include <stdio.h>
#include <stdlib.h>

struct arc{
    int to, from, capacity;
    struct arc *next;
};

typedef struct arc *arcs;

