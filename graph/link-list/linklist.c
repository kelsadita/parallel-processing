#include<stdlib.h>
#include<stdio.h>

struct list_el {
   int val, a;
   struct list_el * next;
};

typedef struct list_el * item;

void main() {
   item curr, tail = NULL;
   int i;


   for(i=1;i<=10;i++) {
      curr       = (item)malloc(sizeof(item));
      curr -> a  = i+1;
      curr->val  = i;
      curr->next = tail;
      tail       = curr;   
   }

    curr = tail;

   while(curr) {
      printf("%d\n", curr->val);
      curr = curr->next ;
   }
}
