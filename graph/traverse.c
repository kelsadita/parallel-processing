#include<stdio.h>
#include<string.h>
#include<math.h>
#include <stdlib.h>


typedef struct node{
	int key;
	struct node *left;
	struct node *right;
}NODE;

NODE* create_node(){
	return malloc(sizeof(NODE));
}
NODE* root;

void insert_node(NODE *p,int k){
	NODE *tmp = create_node();
    tmp -> key = k;

	if( k > p -> key ) {
		if( p -> right != NULL )
			insert_node(p -> right,k);
		else {
			p -> right = tmp;
			printf("Node %d inserted successfully!\n", k);
		}
	} else {
		if( p -> left != NULL )
			insert_node(p -> left,k);
		else {
			p -> left = tmp;
			printf("Node %d inserted successfully!\n",k);
		}
	}
}

void inOrder(NODE* p) {
	if(p -> left != NULL)
		inOrder(p -> left);
	printf(" %d",p -> key);
	if(p -> right != NULL)
		inOrder(p -> right);
}

void preOrder(NODE* p) {
	printf(" %d",p -> key);
	if(p -> left != NULL)
		preOrder(p -> left);
	if(p -> right != NULL)
		preOrder(p -> right);
}

void postOrder(NODE* p) {
	if(p -> left != NULL)
		postOrder(p -> left);
	if(p -> right != NULL)
		postOrder(p -> right);
	printf(" %d",p -> key);
}

int main(int argc, char* argv[]){
    int i, temp;
    NODE *n;
    root = create_node();
    root -> key = 1;
    
    printf("Enter the information of the nodes : \n");
    for(i=2;i<=10;i++){
        //insert_node(root,i);
        scanf("%d", &temp);
        insert_node(root, temp);
    }
    printf("In-order traversal is as follows ... \n");
    inOrder(root);
    printf("\n\n");
    printf("Pre-order traversal is as follows ... \n");
    preOrder(root);
    printf("\n\n");
    printf("Post-order traversal is as follows ... \n");
    postOrder(root);
    printf("\n\n");
    return 0;
}


