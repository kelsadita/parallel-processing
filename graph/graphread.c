#include "graphread.h"

int main(int argc, const char *argv[]){
    
    
    int i, vertices, edges, t_from, t_to, t_capacity;
    arcs curr, head;

    head = NULL;

    /*reading the graph from the file
     * getting number of vertices and edges
     */

    FILE *fp;
    fp = fopen("graph.txt", "r");   
    fscanf(fp, "%d\t%d", &vertices, &edges);

    /* creating a linked list for the number edges in graph */

    for (i = 0;i < edges; i++) {
        
        fscanf(fp, "%d\t%d\t%d", &t_from, &t_to, &t_capacity);
        curr = (arcs)malloc(sizeof(struct arc)); 
        curr -> from     = t_from;
        curr -> to       = t_to;
        curr -> capacity = t_capacity;
        curr -> next     = head;
        head             = curr;
    }

    curr = head;

    while(curr) {
        printf("to : %d, from: %d, capacity: %d\n", curr-> to, curr -> from, curr -> capacity);
        curr = curr->next ;
    }

    fclose(fp);
    return 0;
}
