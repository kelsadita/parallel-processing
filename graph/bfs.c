#include<stdio.h>
#include<stdlib.h>
#include <string.h>

struct edge{

    int vertexIndex;
    struct edge *edgePtr;
}edge;

struct vertex{

    int vertexKey;
    struct edge *edgePtr;
}vertex;

/* Creating array to hold the vertex in the graph */
struct vertex graph[10];
int vertexCount=0;

/* Adding vertex in the structure array */
void InsertVertex(int vertexKey){
    
    int i, present = 0;
    for (i = 0; i < 10; i++) {
        
        if (graph[i].vertexKey == vertexKey) {
           present = 1; 
        }
    }
    
    if (present == 0) {
        
        graph[vertexCount].vertexKey=vertexKey;
        graph[vertexCount].edgePtr=NULL;
        vertexCount++;
    }
    
}


void insertEdge(int vertex1, int vertex2){

    struct edge *e,*e1;
    e = graph[vertex1].edgePtr;
    
    while(e && e->edgePtr){

        e = e->edgePtr;
    }

    e1 = (struct edge *)malloc(sizeof(*e1));
    e1->vertexIndex = vertex2;
    e1->edgePtr = NULL;
    
    if(e)
        e->edgePtr = e1;
    else
        graph[vertex1].edgePtr = e1;

}

void printGraph(){

    int i;
    struct edge *e;
    for(i = 0; i < vertexCount; i++){

        printf("%d(%d)", i, graph[i].vertexKey);
        e = graph[i].edgePtr;
        while(e){

            printf("->%d",e -> vertexIndex);
            e = e -> edgePtr;
        }
        printf("\n");
    }
}

int bfs(int key, int num_vertices){

    int open[num_vertices];
    int close[num_vertices];
    int temp_root, vertex_index, vertex, i ,top = 0;

    for (i = 0; i < num_vertices; i++) {
        open[i]  = 0;
        close[i] = 0;
    }

    struct edge *k;
    temp_root = graph[0].vertexKey;
    open[0]   = temp_root;

    int temp_counter = 0;
    while (open[0] != 0) {

        if (key == open[top]) {
            if(top != 0){
                open[top] = 0;
                top --;
            }
            return 1;
        }else{
            
            //pointing to the node to be expanded
            for (i = 0; i < num_vertices; i++) {
                if (open[top] == graph[i].vertexKey){
                    k            = graph[i].edgePtr;
                    open[top]    = 0;
                    break;
                }
            }
            
            //if node does not have any children
            if (k == NULL) {
                top --;
            }

            //getting the children of the node
            while(k){
                
                vertex_index = k -> vertexIndex;
                open[top]    = graph[vertex_index].vertexKey;
                k            = k -> edgePtr;
                if (k != NULL) {
                    top ++;
                }
            }

           temp_counter ++; 
        }
    }
    return 0;
}
int main(){

    int num_vertices, num_edges, linenumber = 0, i, root, key, found = 0;
    char *cvert   = NULL;
    char *evert   = NULL;
    char buf[30];
    char delims[] = " ";

    /* File read for the information about the grpah */
    FILE *fp;
    fp = fopen("bgraph.txt", "r");   
    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);

    while (fgets(buf, sizeof buf, fp) != NULL) {
        if (linenumber == 2) {

           cvert = (char *)strtok(buf, delims);
           while(cvert != NULL){
               
               InsertVertex(atoi(cvert));
               cvert = (char *)strtok(NULL, delims);
           }

        }else if(linenumber > 3){
            
            evert = (char *)strtok(buf, delims);
            int counter = 0;
            
            while(evert != NULL){
                if (counter == 0) {
                    
                    root = atoi(evert);
                    evert = (char *)strtok(NULL, delims);
                    counter ++;
                }else{
                
                    insertEdge(root, atoi(evert));
                    evert = (char *)strtok(NULL, delims);
                }

            }
        }
        linenumber ++;
    }
   
    printf("The graph is as follows : \n");
    printGraph();

    printf("\n\nEnter the key to be find in the graph : ");
    scanf("%d", &key);

    found = bfs(key, num_vertices);
    if (found == 0) {
        printf("The key is not found in the graph\n");
    }else {
        printf("The key is found in the graph\n");
    }

    return 0;
}
