#include<stdio.h>
#include<stdlib.h>

struct edge{

    int vertexIndex;
    struct edge *edgePtr;
}edge;

struct vertex{

    int vertexKey;
    struct edge *edgePtr;
}vertex;

/* Creating array to hold the vertex in the graph */
struct vertex graph[10];
int vertexCount=0;

/* Adding vertex in the structure array */
void InsertVertex(int vertexKey){
    
    int i, present = 0;
    for (i = 0; i < 10; i++) {
        
        if (graph[i].vertexKey == vertexKey) {
           present = 1; 
        }
    }
    
    if (present == 0) {
        
        graph[vertexCount].vertexKey=vertexKey;
        graph[vertexCount].edgePtr=NULL;
        vertexCount++;
    }
    
}


void insertEdge(int vertex1, int vertex2){

    struct edge *e,*e1;
    e = graph[vertex1].edgePtr;
    
    while(e && e->edgePtr){

        e = e->edgePtr;
    }

    e1 = (struct edge *)malloc(sizeof(*e1));
    e1->vertexIndex = vertex2;
    e1->edgePtr = NULL;
    
    if(e)
        e->edgePtr = e1;
    else
        graph[vertex1].edgePtr = e1;

}

void printGraph(){

    int i;
    struct edge *e;
    for(i=0;i<vertexCount;i++){

        printf("%d(%d)",i,graph[i].vertexKey);
        e=graph[i].edgePtr;
        while(e){

            printf("->%d",e->vertexIndex);
            e=e->edgePtr;
        }
        printf("\n");
    }
}

int main(){

    int vertices, edges;

    /* File read for the information about the grpah */
    FILE *fp;
    fp = fopen("graph.txt", "r");   
    fscanf(fp, "%d\t%d", &vertices, &edges);

    /* Vertices in the graph */
    InsertVertex(5);
    InsertVertex(3);
    InsertVertex(4);
    InsertVertex(2);
    InsertVertex(9);


    /* Edges in the graph */
    insertEdge(0,1);
    insertEdge(0,2);
    insertEdge(1,3);
    insertEdge(1,4);
    printGraph();
    return 0;
}
