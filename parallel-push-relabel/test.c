#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int **alloc_2d_int(int rows, int cols) {
    int *data = (int *)calloc(rows*cols,sizeof(int));
    int **array= (int **)calloc(rows,sizeof(int*));
    int i;
    for ( i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

    return array;
}

void master() {
 int **f = alloc_2d_int(9,9);
	//strcpy(f, "input.txtsad");
 f[8][8] = 88;
 f[4][5] = 45;
 int *f1 = (int *)calloc(9, sizeof(int ));
 f1[8] = 191;
 printf("%d\n", f[0][0] );
 MPI_Send(&f[0][0], 9*9, MPI_INT, 1, 1, MPI_COMM_WORLD);
}

void slave() {
	int **f = alloc_2d_int(9,9);
	//strcpy(f, "input.txtsad");
 
 	int *f1 = (int *)calloc(9, sizeof(int ));
	MPI_Recv(&f[0][0], 9*9, MPI_INT, 0 , 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	printf("%d\n", f[4][4]);
	printf("%s\n", "SLAVE TERMINATES");
}

int main(int argc, char const *argv[])
{
	int rank, num_procs;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	switch(rank){
		case 0:
		printf("%s\n", "Master begins");
		master();
		break;
		case 1:
		printf("%s\n", "Slave begins");
		slave();
		break;
	}
	
	MPI_Finalize();
	return 0;
}