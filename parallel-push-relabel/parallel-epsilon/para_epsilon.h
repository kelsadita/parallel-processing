#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>
#include <unistd.h>

#define MIN(X,Y) X < Y ? X : Y
#define INFINITE 10000000
#define EPSILON 1
#define LB 0

int num_vertices, num_edges;
int *inqueue, *outqueue; 
int *capacities_copy, *flow_copy;

FILE *fo;

// Matrix operations
void print_matrix(int **);
int *get_mat_column(int, int **, int *);

// Suplementary Methods for parallel version
int min_inflow(int, int **);

// Methods for the actual algorithm implementation
int push(int **, int **, int, int, int);
void relabel(int **, int **, int *, int);
int push_relabel(int **, int **, int, int);

void preflow(int **, int **, int);

void send_flow(int **, int **, int *, int *, int);
void rearrange(int, int *);
