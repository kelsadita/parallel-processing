#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MIN(X,Y) X < Y ? X : Y
#define INFINITE 10000000

int num_vertices, num_edges;
FILE *fo;

void print_matrix(int **);
int push(int **, int **, int *, int, int);
void relabel(int **, int **, int *, int);
int push_relabel(int **, int **, int, int);
void preflow(int **, int **, int *, int);
void send_flow(int **, int **, int *, int *, int);
void rearrange(int, int *);
