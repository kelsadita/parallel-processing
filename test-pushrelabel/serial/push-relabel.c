#include "push-relabel.h"

void print_mat(int **mat) {
    int i, j;
    
	fprintf(fo,"------------------------------\n");

    for (i = 0; i < num_vertices; i++) {
        for (j = 0; j < num_vertices; j++)
            if (mat[i][j] > 0) {
                fprintf(fo, "from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    fprintf(fo, "\n");
	fprintf(fo,"------------------------------\n\n");
}

//to provide initial preflow from source to connected vertices
void preflow(int **c, int **f, int *excess, int source){
    
    for (int i = 0; i < num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            f[i][source] -= c[source][i];
            excess[i]     = c[source][i];
            excess[source] -= c[source][i];
        }
    }
}

//push operation is easy to understand
int push(int **c, int **f, int *excess, int u, int v){
    int push_flow = MIN(excess[u], c[u][v] - f[u][v]);
    f[u][v]   += push_flow;
    f[v][u]   -= push_flow;
    excess[u] -= push_flow;
    excess[v] += push_flow;

    return push_flow;
}

//relabeling operation will increase the height of the
//active vertex w.r.t the connected vertex with minimum height
//and for which the edege is following capacity and flow constraints
//I understood relabeling from here 
//https://www.dropbox.com/s/c13pyiwqajyvmg3/Maximum_flow%20%281%29.ppt

void relabel(int **c, int **f, int *height, int u){

    int min_height = INFINITE;
    for (int v = 0; v < num_vertices; v++) {
        if (c[u][v] - f[u][v] > 0) {
            min_height = MIN(min_height, height[v]);
            height[u] = min_height + 1;
        }
    }
}

//required push and relabel operations are performed in this function
//as shown all the vertices connected to active vertex are checked for
//push of flow. Height constraints are also included.
void send_flow(int **c, int **f, int *excess, int *height, int u){
    int v = 0, sent;
    while(excess[u] > 0){
        if (v < num_vertices) {
            if (( c[u][v] - f[u][v] > 0 ) && ( height[u] > height[v] )) {

                fprintf(fo, "Active vertex found : %d\n", u);
                fprintf(fo, "Relabeled %d to : %d\n", u, height[u]);
                sent = push(c, f, excess, u, v);
                fprintf(fo, "%d-[%d]->%d\n", u, sent, v);

            }
            v ++;
        }else {
            relabel(c, f, height, u);
            v = 0;
        }
    }
}

//function is called to reinitialize the active vertex routine
//in this the processed vertex brought to front and the remaining 
//vertices are checked again for activeness in further routine
void rearrange(int k, int *intermediate){
    int temp = intermediate[k];
    for (int j = k; j > 0 ; j--) {
        intermediate[j] = intermediate[j - 1];
    }
    intermediate[0] = temp;
}

int push_relabel(int **c, int **f, int source, int sink){

    int i, j = 0, old_height, sum = 0;
    int *excess       = (int *)calloc(num_vertices, sizeof(int));
    int *excess1       = (int *)calloc(num_vertices, sizeof(int));
    int *height       = (int *)calloc(num_vertices, sizeof(int));
    int *height1       = (int *)calloc(num_vertices, sizeof(int));
    int *intermediate = (int *)calloc(num_vertices, sizeof(int));
    for (i = 0; i < num_vertices*2; i++) {
        for (j = 0; j < num_vertices*2; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    excess1[k] = j;
                    height1[k] = k;
            }
        }           
    }
  
    //initializing with preflow
    preflow(c, f, excess, source);

    //initializing the height of source
    height[source] = num_vertices;

    //getting intermediate nodes for processing
    for (i = 0; i < num_vertices; i++) {
        if (i != 0 && i != num_vertices - 1) {
            intermediate[i - 1] = i;
        }
    }

    
    //performing operations considering single intermediate vertex at a time
    fprintf(fo, "Format : from-[sent flow]->to\n");
    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    excess1[k] = k;
                    height1[k] = i;
            }
        }           
    }
    
    for (i = 0; i < num_vertices - 2; i++) {
        int u = intermediate[i];
        old_height = height[u];
        //function will check if the provided vertex is active or not.
        //in the case of active it will push the flow accordingly
        send_flow(c, f, excess, height, u);
        //when old_height is lesser than the new one the active vertex has been found,
        //the graph's flow has been changed and we need to reinitialize the the algorithm
        //because there is a possibility that the vertex which has been processed earlier
        //may become active again.
        if (old_height < height[u]) {
            rearrange(i, intermediate);
            i = 0;
        }
    }

    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    excess1[k] = i;
                    height1[k] = j;
            }
        }           
    }
    //final labels on the vertices
    fprintf(fo, "\nFinal heights : \n");
    fprintf(fo, "------------------------------\n");
    
    for (i = 0; i < num_vertices; i++) {
        fprintf(fo, "Height[%d] : %d\n", i, height[i]);
    }
    /**
     *
     * **/

    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    excess1[k] = k;
                    height1[k] = j;
            }
        }           
    }
    fprintf(fo, "------------------------------\n");
    //Maximum flow obtained at the sink
    
    for (i = 0; i < num_vertices; i++) {
        if (f[i][num_vertices - 1] > 0) {
            sum += f[i][num_vertices - 1];
        }
    }
    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    excess1[k] = 0;
                    height1[k] = 0;
            }
        }           
    }
    fprintf(fo, "Maximum flow : %d\n", sum);
    
}

int main(int argc, const char *argv[]) {

    clock_t tic = clock();   

    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    FILE *fp;
    if (argv[1] == NULL) {
        printf("Format is : ./push-relabel <graphfile>\n");
        exit(0);
    }else {
        //output file
        fo = fopen("output.txt","w");
        //input file
        fp = fopen(argv[1], "r");   
    }

    //getting number of vertices and edges
    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);
    
    //initializing the flows and the capacities
    flow       = (int **) calloc(num_vertices, sizeof(int*));
    capacities = (int **) calloc(num_vertices, sizeof(int*));

    for (i = 0; i < num_vertices; i++) {
        flow[i]       = (int *) calloc(num_vertices, sizeof(int));
        capacities[i] = (int *) calloc(num_vertices, sizeof(int));
    }

    //getting the info about capacities and edges
    for (i = 0; i < num_edges; i++) {

        fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
        capacities[from][to] = capacity;
    }
    
    //printing the capacities of the matrix
    fprintf(fo, "\nCapacities:\n");
    print_mat(capacities);

    push_relabel(capacities, flow, 0, num_vertices - 1);

    //printing the final flow in the network
    fprintf(fo, "\nFlows:\n");
    print_mat(flow);

    clock_t toc = clock();
    fprintf(fo, "Execution time : %f seconds\n", (double)(toc - tic)/1000000);
	fprintf(fo,"------------------------------\n");
    //system("cat output.txt");
    return 0;
}
