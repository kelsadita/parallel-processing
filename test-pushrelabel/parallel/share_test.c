#include <mpi.h>
#include <stdio.h>

int s[6] = {1, 2, 3, 4, 5, 6};

int main(int argc, const char *argv[])
{
    MPI_Init(NULL, NULL);
    
    int size, rank;
    

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 2) {
        s[3] = 100;
        printf("For process %d the value for s[3] is : %d\n", rank, s[3]);
    }else{
        
        printf("For process %d the value for s[3] is : %d\n", rank, s[3]);
    }
    

    MPI_Finalize();
    
    return 0;

}
