#include "para_push.h"

int **alloc_2d_int(int rows, int cols) {
        int *data = (int *)calloc(rows*cols,sizeof(int));
        int **array= (int **)calloc(rows,sizeof(int*));
        int i;
        for ( i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

        return array;
}

int **mat_sum(int **mat1, int **mat2) {
        int **mat = alloc_2d_int(num_vertices+1, num_vertices+1);
        for (int i = 1; i <= num_vertices; ++i)
         {
                 for (int j = 1; j <= num_vertices; ++j)
                 {
                         mat[i][j] = mat1[i][j] + mat2[i][j];
                 }
         } 
         return mat;
}

void print_mat( int **mat){
        printf("\n-------------------------------------------\n");
        for (int i = 1; i <= num_vertices; ++i)
         {
                 for (int j = 1; j <= num_vertices; ++j)
                 {
                         printf("\t%d", mat[i][j]);
                 }
                 printf("\n");
         }
}
void master_process( char *graph ) {
        FILE *fp = fopen( graph, "r");
        int from, to, max_capacity;
        fscanf(fp, "%d\t%d", &num_vertices, &num_edges);
        int **capacity = alloc_2d_int(num_vertices+1, num_vertices+1);
        mat_size = (num_vertices+1)*(num_vertices+1);
        for (int i = 0; i < num_edges; ++i)
        {
                fscanf(fp,"%d\t%d\t%d", &from, &to, &max_capacity);
                capacity[from][to] = max_capacity; 
        }
        fclose(fp);
        for (int i = 1; i <= num_vertices; ++i)
        {
	      MPI_Send(&num_vertices, 1, MPI_INT, i, 5, MPI_COMM_WORLD);        
	      MPI_Send(&capacity[0][0], mat_size, MPI_INT, i, 1, MPI_COMM_WORLD);        
        }
       // printf("%s\n", "Check point :  capacity sent [ OK ]");
        //exit(0);
}

void source_process(int sink) {
	MPI_Recv(&num_vertices, 1, MPI_INT, 0, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        mat_size = (num_vertices+1)*(num_vertices+1);
        int **capacity = alloc_2d_int(num_vertices+1, num_vertices+1);
        int **flow = alloc_2d_int(num_vertices+1, num_vertices+1);
        int **flow_tmp = alloc_2d_int(num_vertices+1, num_vertices+1);
        int *excess = (int *)calloc(num_vertices+1, sizeof(int));
        int node_excess = 0;
        MPI_Recv(&capacity[0][0], mat_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        //printf("%s%d [ OK ]\n", "Check point :  capacity received at ", 1);
        for (int i = 2; i <= num_vertices; ++i)
        {
                if(capacity[1][i] != 0 ) 
                {
                        flow[1][i] += capacity[1][i];
                        flow[i][1] -= capacity[1][i];
                        excess[1] -= capacity[1][i]; 
                        excess[i] += capacity[1][i];
                        node_excess -= capacity[1][i];
                        MPI_Send(&excess[i], 1 , MPI_INT, i, 1, MPI_COMM_WORLD);
                }  
        }
        int incoming_excess = 0;
        for (int i = sink - 1; i > 1; --i)
        {
            if( flow[1][i] != 0 ){
		MPI_Recv(&incoming_excess, 1, MPI_INT, i, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);            
            	node_excess += incoming_excess;
            	flow[1][i] -= incoming_excess;
            	flow[i][1] += incoming_excess;
            	excess[1] +=  incoming_excess;
	    }
        }
        for (int i = 2; i < sink; ++i)
        {
                 
		MPI_Recv(&flow_tmp[0][0], mat_size, MPI_INT, i, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                flow = mat_sum(flow, flow_tmp);
		/*	
                MPI_Recv(&tmp[0], num_vertices+1, MPI_INT, i, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("*************Received finall flow from process %d ...\n",i);
		for(int j = 1; j <= sink; j++){
			printf(" %d",tmp[j]);
		}
		printf("\n");
		*/
        }
//        printf("%s%d [ OK ]\n", "Check point :  END OF SOURCE ", 1);
  //      print_mat(flow_tmp);
}

void intermediate_process(int rank, int sink) {
	MPI_Recv(&num_vertices, 1, MPI_INT, 0, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        mat_size = (num_vertices+1)*(num_vertices+1);
        int **capacity = alloc_2d_int(num_vertices+1, num_vertices+1);
        int **flow = alloc_2d_int(num_vertices+1, num_vertices+1);
        int node_excess = 0;
        int *excess = (int *)calloc(num_vertices+1, sizeof(int));
        MPI_Recv(&capacity[0][0], mat_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
       // printf("%s%d [ OK ]\n", "Check point :  capacity received at ", rank);
        if( capacity[1][rank] != 0 )
            MPI_Recv(&node_excess, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        int incoming_excess;
        for (int i = 2; i < rank; ++i)
        {
		if(capacity[i][rank] != 0){
	            MPI_Recv(&incoming_excess, 1, MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);            
        	    node_excess += incoming_excess;
		}
        }
        //printf("%s%d [ OK ]\n", "Check point :  excess received at ", rank);
        int flow_pushed;
        for (int i = rank+1 ; i <= num_vertices; ++i)
        {
                if(capacity[rank][i] != 0 ) 
                {
			//printf("***********Flow pushed to %d from %d**************",i,rank);
                    if ( node_excess != 0)
                    {
                        flow_pushed = MIN(capacity[rank][i], node_excess);
                        flow[rank][i] += flow_pushed;
                        flow[i][rank] -= flow_pushed;
                        excess[rank] -= flow_pushed; 
                        excess[i] += flow_pushed;
                        node_excess -= flow_pushed;
                        MPI_Send(&excess[i], 1 , MPI_INT, i, 2, MPI_COMM_WORLD);
                    } 
                    else
                    {
                        flow_pushed = 0;
                        MPI_Send(&flow_pushed, 1 , MPI_INT, i, 2, MPI_COMM_WORLD);
                    }
                        
                }
                
        }
        //printf("%s%d [ OK ]\n", "Check point :  Push operation performed at ", rank);
        for (int i = sink - 1; i > rank; --i)
        {
		if(capacity[rank][i] != 0){
	            MPI_Recv(&incoming_excess, 1, MPI_INT, i, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);            
        	    node_excess += incoming_excess;
		}
        }
       // printf("%s%d [ OK ]\n", "Check point :  reverse excess received at ", rank);
        
        for (int i = rank - 1 ; i >= 1; --i)
        {
                if(capacity[i][rank] != 0 ) 
                {
                        if ( node_excess != 0)
                        {
                                flow_pushed = MIN(capacity[i][rank], node_excess);
                                flow[i][rank] -= flow_pushed;
                                flow[rank][i] += flow_pushed;
                                excess[rank] -= flow_pushed; 
                                excess[i] += flow_pushed;
                                node_excess -= flow_pushed;
                                MPI_Send(&excess[i], 1 , MPI_INT, i, 3, MPI_COMM_WORLD);
                        } 
                        else
                        {
                                flow_pushed = 0;
                                MPI_Send(&flow_pushed, 1 , MPI_INT, i, 3, MPI_COMM_WORLD);
                        }               
                }         
        }
        MPI_Send(&flow[0][0],mat_size, MPI_INT, 1, 4, MPI_COMM_WORLD);
}

void sink_process() {
	MPI_Recv(&num_vertices, 1, MPI_INT, 0, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        mat_size = (num_vertices+1)*(num_vertices+1);
        int **capacity = alloc_2d_int(num_vertices+1, num_vertices+1);
        MPI_Recv(&capacity[0][0], mat_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        int incoming_excess, node_excess = 0;
	if(capacity[1][num_vertices] != 0) {
        	MPI_Recv(&incoming_excess, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        	node_excess += incoming_excess;
	}
	//printf("***********FLOW RECEIVED AT SINK***********\n");
        for (int i = 2; i < num_vertices; ++i)
        {
		if(capacity[i][num_vertices] != 0){
			//printf("##########Excess received from %d#############",i);
            		MPI_Recv(&incoming_excess, 1, MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);            
            		node_excess += incoming_excess;
		}
        }
        printf("Maximum flow : %d\n", node_excess);
        //exit(0);
}

int main(int argc, char *argv[])
{
        int rank, num_procs;
        //char processor_name[MAX_NAME_LEN];
        //int name_len = MAX_NAME_LEN;
        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        //MPI_Get_processor_name(processor_name, &name_len);

        if (argv[1] == NULL)
        {
                        printf("%s\n", "Invalid input file      [ OK ]");
                        exit(0);
        }
        //printf("%s\n", "Check point : num_vertices read   [ OK ]");
        if (rank == 0)
        {
   //             printf("%s\n", "Check point :  Master Initiate [ OK ]");
                master_process(argv[1]);
        } 
        else  
        {
                if ( rank == 1 ){
     //                   printf("%s\n", "Check point :  source_process Initiate [ OK ]");
                        source_process(num_vertices);
       //                 printf("%s%d [ OK ]\n", "Check point :  Control returns at process ", rank);
                }
                else if( rank != num_procs - 1 ) {
         //               printf("Check point : Slave %d initiates [ OK ]\n", rank);
                        intermediate_process(rank, num_vertices);
           //             printf("%s%d [ OK ]\n", "Check point :  Control returns at process ", rank);
                }
                else {
             //           printf("Check point :  sink Initiate [ OK ]\n");
                        sink_process();
               //         printf("%s%d [ OK ]\n", "Check point :  Control returns at process ", rank);
                }
        }

        MPI_Finalize();
        return 0;

}
