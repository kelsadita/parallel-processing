#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#define MAX_NAME_LEN 10
#define INFINITE 1000000
#define MIN(X,Y) X < Y ? X : Y

int num_vertices, num_edges, mat_size;
FILE *fo;
