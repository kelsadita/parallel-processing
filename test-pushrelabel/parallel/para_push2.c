#include "para_push2.h"

// Printing the matrix values
void print_mat(int **mat) {
    int i, j;
    
    printf("------------------------------\n");

    for (i = 0; i < num_vertices; i++) {
        for (j = 0; j < num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
    printf("------------------------------\n\n");
}

// getting the column of a matrix
int *get_mat_column(int index, int **mat, int *temp){

    int count = 0;
    for (int i = 0; i < num_vertices; ++i){
        
        temp[i] = mat[count][index];
        count ++;
    }

    return temp;
}

// getting the column sum for particular index of the matrix
int get_mat_column_sum(int index, int **mat){

    int sum = 0;
    for (int i = 0; i < num_vertices; ++i){
        
        sum += mat[i][index];
    }

    return sum;
}

/*--------------CODE OF ACTUAL SERIAL ALGO---------------------*/

// to provide initial preflow from source to connected vertices  
void preflow(int **c, int **f, int source){
    
    for (int i = 0; i < num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
      
        }
    }
}


// Used in parallel pushes
int par_push(int **c, int **f, int excess, int u, int v){
    
    // getting the amount of flow to be pushed
    int push_flow = MIN(excess, c[u][v] - f[u][v]);
    
    f[u][v]   += push_flow;
    return push_flow;
}

// Used for remaining pushes
int push(int **c, int **f, int *excess, int u, int v){
    int push_flow = MIN(excess[u], c[u][v] - f[u][v]);
    f[u][v]   += push_flow;
    f[v][u]   -= push_flow;
    excess[u] -= push_flow;
    excess[v] += push_flow;

    return push_flow;
}

void relabel(int **c, int **f, int *height, int u){

    int min_height = INFINITE;
    for (int v = 0; v < num_vertices; v++) {
        if (c[u][v] - f[u][v] > 0) {
            min_height = MIN(min_height, height[v]);
            height[u] = min_height + 1;
        }
    }
}

void send_flow(int **c, int **f, int *excess, int *height, int u){
    int v = 0, sent;
    while(excess[u] > 0){
        if (v < num_vertices) {
            if (( c[u][v] - f[u][v] > 0 ) && ( height[u] > height[v] )) {

                fprintf(fo, "Active vertex found : %d\n", u);
                fprintf(fo, "Relabeled %d to : %d\n", u, height[u]);
                sent = push(c, f, excess, u, v);
                fprintf(fo, "%d-[%d]->%d\n", u, sent, v);

            }
            v ++;
        }else {
            relabel(c, f, height, u);
            v = 0;
        }
    }
}

void rearrange(int k, int *intermediate){
    int temp = intermediate[k];
    for (int j = k; j > 0 ; j--) {
        intermediate[j] = intermediate[j - 1];
    }
    intermediate[0] = temp;
}

int push_relabel(int **c, int **f, int *excess, int source, int sink){

    int i, j = 0, old_height, sum = 0;
    int *height       = (int *)calloc(num_vertices, sizeof(int));
    int *intermediate = (int *)calloc(num_vertices, sizeof(int));

    //initializing the height of source
    height[source] = num_vertices;

    //getting intermediate nodes for processing
    for (i = 0; i < num_vertices; i++) {
        if (i != 0 && i != num_vertices - 1) {
            intermediate[i - 1] = i;
        }
    }

    
    //performing operations considering single intermediate vertex at a time
    fprintf(fo, "Format : from-[sent flow]->to\n");

    for (i = 0; i < num_vertices - 2; i++) {
        int u = intermediate[i];
        old_height = height[u];
        //function will check if the provided vertex is active or not.
        //in the case of active it will push the flow accordingly
        send_flow(c, f, excess, height, u);
        //when old_height is lesser than the new one the active vertex has been found,
        //the graph's flow has been changed and we need to reinitialize the the algorithm
        //because there is a possibility that the vertex which has been processed earlier
        //may become active again.
        if (old_height < height[u]) {
            rearrange(i, intermediate);
            i = 0;
        }
    }

    //final labels on the vertices
    fprintf(fo, "\nFinal heights : \n");
    fprintf(fo, "------------------------------\n");
    
    for (i = 0; i < num_vertices; i++) {
        fprintf(fo, "Height[%d] : %d\n", i, height[i]);
    }

    fprintf(fo, "------------------------------\n");
    //Maximum flow obtained at the sink
    
    for (i = 0; i < num_vertices; i++) {
        if (f[i][num_vertices - 1] > 0) {
            sum += f[i][num_vertices - 1];
        }
    }
   
    fprintf(fo, "Maximum flow : %d\n", sum);
    
}



/*-------------------------------------------------------------*/

int main(int argc, char const *argv[])
{
    clock_t tic = clock();

    MPI_Init(NULL, NULL);
    
    int size, rank;
    
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

    double start = MPI_Wtime();
    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    // Master
    //initializing the flows and the capacities
    flow       = (int **) calloc(size, sizeof(int*));
    capacities = (int **) calloc(size, sizeof(int*));

    for (i = 0; i < size; i++) {
        flow[i]       = (int *) calloc(size, sizeof(int));
        capacities[i] = (int *) calloc(size, sizeof(int));
    }

    if (rank == 0){

        FILE *fp;
        if (argv[1] == NULL) {

            exit(0);
        
        }else {
        
            //output file
            fo = fopen("output.txt","w");
    
            //input file
            fp = fopen(argv[1], "r");   
        }


        // getting number of vertices and edges
        fscanf(fp, "%d\t%d", &num_vertices, &num_edges);

        // number of processes should be equal to number of vertices
        if (num_vertices != size) {
            printf("Number of processes should be %d\n", num_vertices);
            printf("Exiting.......\n");
            exit(0);
        }


        // getting the info about capacities and edges
        for (i = 0; i < num_edges; i++) {

            fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
            capacities[from][to] = capacity;
        }
        
        // printing the capacities of the matrix
        printf("\nCapacities:\n");
        print_mat(capacities);

        preflow(capacities, flow, 0);

        //--TESTING
         printf("\nAfter preflow:\n");
         print_mat(flow);

     
    }

    // broadcasting number of vertices and edges to the other processes.
    MPI_Bcast(&num_vertices, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_edges, 1, MPI_INT, 0, MPI_COMM_WORLD);
   

    // broadcating the capacities and flows to other processes
    for (i = 0; i < num_vertices; i++) {
        
        MPI_Bcast(&capacities[i][0], num_vertices, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&flow[i][0], num_vertices, MPI_INT, 0, MPI_COMM_WORLD);
    }


    if ((rank != 0) && (rank != num_vertices - 1)){

        // local variables required for the other processes
        int *loc_outqueue;
        loc_outqueue = flow[rank];
   
        
        // int capacities[num_vertices][num_vertices];
        int *send_master, *source_nodes;

        //to send to the receiving vertex
        int pushed_amount;
        
        send_master    = (int *) calloc(num_vertices, sizeof(int));
        source_nodes   = (int *) calloc(num_vertices, sizeof(int));

        int in_sum = 0, out_sum = 0, index = 0, i, j, excess;
        int vertex_out, vertex_in;

        // pushing the flow from the process where it is possible
        source_nodes = get_mat_column(rank, capacities, source_nodes);
        for (int i = 0; i < num_vertices; ++i){
           
            if (source_nodes[i] != 0){
              
                if (flow[i][rank] == 0) {
                   
                    MPI_Recv(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    //TESTING
                    //printf("I am vertex %d and from '%d' i received : %d\n", rank + 1, i + 1, pushed_amount);
                
                    // updating the flow matrix
                    flow[i][rank] += pushed_amount;
                }
            }
        }


        in_sum = get_mat_column_sum(rank, flow);

        for (int i = 0; i < num_vertices; i++) {
            
            out_sum += loc_outqueue[i];
        }


        excess = in_sum - out_sum;
        for (i = 1; i < num_vertices; i++) {
            if(capacities[rank][i] != 0){
                
                // TESTING 
                //printf("for vertex %d surplus is %d\n", rank + 1, surplus);

                pushed_amount = par_push(capacities, flow, excess, rank, i);

                // TESTING
                //printf("vertex %d sending to %d the flow %d\n", rank + 1, i, pushed_amount);

                // TESTING
                //printf("Rank %d sending to %d the flow %d \n", rank, i - 1, pushed_amount);
                
                if (i != num_vertices - 1) {
                    MPI_Send(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                }
                excess -= pushed_amount;

                // TESTING
                //printf("Surplus after push %d\n", surplus);
            }
        }
        
        /*TESTING
        *printf("For process %d\n", rank);
        *printf("local flows :\n");
        *print_mat(flow);
        */

        // sending updated flows and surplus to the master
        send_master = flow[rank];
        MPI_Send(&send_master[0], num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&excess, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  
    }

    if(rank == 0){
        int *updated_flow, *excess, temp_surplus;

        updated_flow = (int *)calloc(num_vertices, sizeof(int));
        excess      = (int *)calloc(num_vertices, sizeof(int));

        // receiving updates from childrens
        for (int i = 1; i < num_vertices - 1; ++i){
           MPI_Recv(&updated_flow[0], num_vertices, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           MPI_Recv(&temp_surplus, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           //printf("From %d the surplus received : %d\n", i, temp_surplus);
           excess[i] = temp_surplus;
           for (int j = 0; j < num_vertices; ++j){
               
               flow[i][j] = updated_flow[j];

                //printf("flow from %d to %d is :%d\n", j, i+1, flow[j][i+1]);
           }
        }

        // master copy of flow
        printf("Master copy of updated flows : \n");
        print_mat(flow);

        for (int i = 0; i < num_vertices; ++i)
        {
            for (int j = 0; j < num_vertices; ++j)
            {
                if (flow[i][j] != 0)
                {
                    flow[j][i] = -flow[i][j];
                }
            }
        }

        // final updation
        push_relabel(capacities, flow, excess, 0, num_vertices - 1);
        // Final flows
        printf("Master copy of updated flows : \n");
        print_mat(flow);
        double end = MPI_Wtime();
        printf("\n----------------------------------\n");
        printf("Excution time : %lf\n\n",end-start);
               
    }
    MPI_Finalize();

}
