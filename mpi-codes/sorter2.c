#include <stdio.h>
#include <mpi.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>


#define MAX_BUFFER_SIZE		100

#define MAXLINE	10

char input_line[MAXLINE];


int Buffer[MAX_BUFFER_SIZE];
int Buffer2[MAX_BUFFER_SIZE];
int count = 0;

void display(int *buffer, int from, int size, int rank){
	int i,partialSum;

	printf("\n\tProcess %d showing : \n[ ",rank);
	for(i=from;i<from+size;i++)
		printf("%d ",buffer[i]);
	printf("]\n");

//	partialSum = add(buffer,from,size);
//	printf("Partial Sum from process %d : %d\n",rank,partialSum);
}

FILE * eatNewlines(char *filename, int start){

	FILE *source = fopen(filename,"r");
	int eat = 0;
	while(eat<start){
		fgets(input_line,MAXLINE,source);
	       	eat++;
	}

	return source;
}


void fillBuffer(char *filename,int start, int length){
	
	int i;
	FILE *f = eatNewlines(filename,start);
	
	for(i=0;i<length;i++)
		fscanf(f,"%d",&Buffer[i]); 
	

}

int getSize(char *filename){

	int numData = 0 ;

	FILE *source = fopen(filename,"r");

	while(fgets(input_line,MAXLINE,source))	 numData++;
	

	fclose(source);
	return numData;
}


int add(int *buffer, int start,int size){
	int sum = 0,i;

	for(i=start;i<start+size;i++)
		sum += buffer[i];

	return sum;


}

void quicksort(int a, int b){
	int rtidx = 0, ltidx = 0, k = a, l = 0 , pivot;
	int leftarr[MAX_BUFFER_SIZE] , rtarr[MAX_BUFFER_SIZE];

	pivot = Buffer[a];

	if(a == b) return;

	while(k<b){
		k++;

		if(Buffer[k] < pivot){
			leftarr[ltidx] = Buffer[k];
			ltidx++;
		}else{
			rtarr[rtidx] = Buffer[k];
			rtidx++;
		}
	}

	k = a;

	for(l=0;l<ltidx;l++) Buffer[k++] = leftarr[l];
	Buffer[k++] = pivot;
	for(l=0;l<rtidx;l++) Buffer[k++] = rtarr[l];

	if(ltidx > 0) quicksort(a, a + ltidx - 1);
	if(rtidx > 0) quicksort(b - rtidx + 1, b);

}





int main(int argc, char **argv){

	int numtasks, rank, dest, source, rc, tag=5,i;
	int cut,temp[2], partialSum = 0, totalSum = 0,psum[3];
	int size1, size2;

//	fillBuffer(argv[1]);
//
//
//	display(Buffer,0,count);
//


//	printf("Number of numbers : %d\n",count);	


	MPI_Status stat;
	MPI_Datatype bound;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	MPI_Type_contiguous(2,MPI_INT,&bound);
	MPI_Type_commit(&bound);

	switch(rank){
		case 0:
	count = getSize(argv[1]);

	cut = count/4;
	temp[0] = 0;
	temp[1] = cut;
	fillBuffer(argv[1],temp[0],temp[1]);
	quicksort(0, temp[1] - 1);
//	display(Buffer,0,temp[1],rank);

	for(i=1;i<4;i++){
	temp[0] = (i)*cut;
	if(i==3)
		temp[1] = cut+count%4;
	else
		temp[1] = cut;
	dest = i;

	rc = MPI_Send(temp,1,bound,dest,tag+dest,MPI_COMM_WORLD);
	}
		break;
		case 1:
	
	rc = MPI_Recv(temp,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
	fillBuffer(argv[1],temp[0],temp[1]);
	quicksort(0, temp[1] - 1);
//	display(Buffer,0,temp[1],rank);

		break;
		case 2:

	rc = MPI_Recv(temp,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
	fillBuffer(argv[1],temp[0],temp[1]);
	quicksort(0, temp[1] - 1);
	display(Buffer,0,temp[1],rank);

	rc = MPI_Send(&temp[1],1,MPI_INT,3,10,MPI_COMM_WORLD);
//	printf("Process : %d -->Sent size2 %d to 3\n",rank,temp[1]);
//	rc = MPI_Send(Buffer,temp[1],MPI_INT,3,10,MPI_COMM_WORLD,&stat);

	rc = MPI_Send(Buffer,temp[1],MPI_INT,3,11,MPI_COMM_WORLD);
		break;
		case 3:
	
	rc = MPI_Recv(temp,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
	fillBuffer(argv[1],temp[0],temp[1]);
	quicksort(0, temp[1] - 1);
 	display(Buffer,0,temp[1],rank);

	rc = MPI_Recv(&size2,1,MPI_INT,2,10,MPI_COMM_WORLD, &stat);
	
	rc = MPI_Recv(Buffer2,size2,MPI_INT,2,11,MPI_COMM_WORLD, &stat);


	display(Buffer2,0,size2,rank);

//	printf("Process : %d -->received size2 %d from 2\n",rank,size2);

//	rc = MPI_Recv(Buffer2,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
		break;
	}

	MPI_Finalize();
}
