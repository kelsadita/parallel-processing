#include <stdio.h>
#include <mpi.h>

#define MAX_BUFFER_SIZE		100

int Buffer[MAX_BUFFER_SIZE];
int count = 0;

void display(int *buffer, int from, int size, int rank){
	int i;

	printf("\n\tProcess %d showing : \n[ ",rank);
	for(i=from;i<from+size;i++)
		printf("%d ",buffer[i]);
	printf("]\n");
}

void fillBuffer(char *filename){
	
	FILE *f = fopen(filename,"r");


	while(fscanf(f,"%d",&Buffer[count]) != EOF){
//		printf("count : %d\tread : %d\n",count,Buffer[count]);
	       	count++;
	}

}

int main(int argc, char **argv){

	int numtasks, rank, dest, source, rc, tag=5;
	int cut,temp[2];


	fillBuffer(argv[1]);
//	display(Buffer,0,count);

	MPI_Status stat;
	MPI_Datatype bound;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	MPI_Type_contiguous(2,MPI_INT,&bound);
	MPI_Type_commit(&bound);

	if(rank == 0){

	display(Buffer,0,count,rank);
	cut = count/3;
	temp[0] = 0;
	temp[1] = cut;
	rc = MPI_Send(temp,1,bound,1,tag+1,MPI_COMM_WORLD);
//	rc = MPI_Send(&temp[0],1,MPI_INT,1,tag+1,MPI_COMM_WORLD);

	temp[0] = cut;
	temp[1] = cut;
	rc = MPI_Send(temp,1,bound,2,tag+2,MPI_COMM_WORLD);
//	rc = MPI_Send(&temp[0],1,MPI_INT,2,tag+2,MPI_COMM_WORLD);

	temp[0] = 2*cut;
	temp[1] = cut+count%3;
	rc = MPI_Send(temp,1,bound,3,tag+3,MPI_COMM_WORLD);
//	rc = MPI_Send(&temp[0],1,MPI_INT,3,tag+3,MPI_COMM_WORLD);
	}
	else{
	rc = MPI_Recv(temp,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
//printf("\nRank %d: \tstart : %d \tlength : %d ",rank,temp[0],temp[1]);

		display(Buffer,temp[0],temp[1],rank);
	}

	MPI_Finalize();
}
