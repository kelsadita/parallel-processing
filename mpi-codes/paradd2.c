#include <stdio.h>
#include <mpi.h>

#define MAX_BUFFER_SIZE		100

int Buffer[MAX_BUFFER_SIZE];
int count = 0;

void display(int *buffer, int from, int size, int rank){
	int i,partialSum;

	printf("\n\tProcess %d showing : \n[ ",rank);
	for(i=from;i<from+size;i++)
		printf("%d ",buffer[i]);
	printf("]\n");

//	partialSum = add(buffer,from,size);
//	printf("Partial Sum from process %d : %d\n",rank,partialSum);
}

void fillBuffer(char *filename){
	
	FILE *f = fopen(filename,"r");


	while(fscanf(f,"%d",&Buffer[count]) != EOF){
//		printf("count : %d\tread : %d\n",count,Buffer[count]);
	       	count++;
	}

}

int add(int *buffer, int start,int size){
	int sum = 0,i;

	for(i=start;i<start+size;i++)
		sum += buffer[i];

	return sum;


}

int main(int argc, char **argv){

	int numtasks, rank, dest, source, rc, tag=5,i;
	int cut,temp[2], partialSum = 0, totalSum = 0,psum[3];


	fillBuffer(argv[1]);
//	display(Buffer,0,count);

	MPI_Status stat;
	MPI_Datatype bound;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	MPI_Type_contiguous(2,MPI_INT,&bound);
	MPI_Type_commit(&bound);

	if(rank == 0){

	display(Buffer,0,count,rank);
	cut = count/3;
	for(i=1;i<4;i++){
	temp[0] = (i-1)*cut;
	if(i==3)
		temp[1] = cut+count%3;
	else
		temp[1] = cut;
	dest = i;

	rc = MPI_Send(temp,1,bound,dest,tag+dest,MPI_COMM_WORLD);
	source = i;
rc = MPI_Recv(&psum[i-1],1,MPI_INT,source,tag+source,MPI_COMM_WORLD, &stat);
	}
/*	for(i=1;i<4;i++){
	source = i;
rc = MPI_Recv(&psum[i-1],1,MPI_INT,source,tag+source,MPI_COMM_WORLD, &stat);
	}
*/
	totalSum = add(psum,0,3);
	printf("Total Sum in process %d : %d\n",rank,totalSum);

	}
	else{
	rc = MPI_Recv(temp,1,bound,0,tag+rank,MPI_COMM_WORLD, &stat);
//printf("\nRank %d: \tstart : %d \tlength : %d ",rank,temp[0],temp[1]);

//		display(Buffer,temp[0],temp[1],rank);
		partialSum = add(Buffer,temp[0],temp[1]);
		printf("Partial Sum from process %d : %d\n",rank,partialSum);

		dest = 0;

rc = MPI_Send(&partialSum,1,MPI_INT,dest,tag+rank,MPI_COMM_WORLD);
	
	}

	MPI_Finalize();
}
