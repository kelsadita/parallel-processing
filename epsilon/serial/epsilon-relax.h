#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MIN(X,Y) X < Y ? X : Y
#define INFINITE 10000000
#define EPSILON 1
#define LB 0

int num_vertices, num_edges;
FILE *fo;

void print_matrix(int **);
void print_admissible(int **);
int push(int **, int **, int *, int, int);
void relabel(int **, int **, int *, int);
int up_iteration(int **, int **, int, int);
void preflow(int **, int **, int *, int);
void send_flow(int **, int **, int *, int *, int);
void rearrange(int, int *);
void generate_pushlist(int, int *, int **, int **, int **, int **);
void incr_price(int, int *, int **, int **, int **);
