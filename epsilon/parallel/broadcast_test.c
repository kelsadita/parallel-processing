#include <stdio.h>
#include <mpi.h>

int main(int argc, char const *argv[])
{
	MPI_Init(NULL, NULL);
    
    int size, rank, s;
    

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        s = 100;
        printf("For process %d the value for s is : %d\n", rank, s);
    }
        
    MPI_Bcast(&s, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    if(rank != 0){
    	if(rank == 5){
    		s = 50;
    		
    	}
    	if (rank == 2)
    	{
    		printf("Hello from 2 %d\n", s);
    	}
    	MPI_Bcast(&s, 1, MPI_INT, 5, MPI_COMM_WORLD);
    	
    	//while(s == 100);
    	printf("For process %d the value for s is : %d\n", rank, s);
    }
    printf("For process %d the value for s is : %d\n", rank, s);
    

    MPI_Finalize();
    
    return 0;

}