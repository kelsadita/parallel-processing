#include "para_epsilon.h"

// Printing the matrix values
void print_mat(int **mat) {
    int i, j;
    
    printf("------------------------------\n");

    for (i = 1; i <= num_vertices; i++) {
        for (j = 1; j <= num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
    printf("------------------------------\n\n");
}

// getting the column of a matrix
int *get_mat_column(int index, int **mat, int *temp){

    int count = 1;
    for (int i = 0; i < num_vertices; ++i){
        
        temp[i] = mat[count][index];
        count ++;
    }

    return temp;
}

// getting the column sum for particular index of the matrix
int get_mat_column_sum(int index, int **mat){

    int sum = 0;
    for (int i = 1; i <= num_vertices; ++i){
        
        sum += mat[i][index];
    }

    return sum;
}

/*--------------CODE OF ACTUAL SERIAL ALGO---------------------*/

// to provide initial preflow from source to connected vertices  
void preflow(int **c, int **f, int source){
    
    for (int i = 1; i <= num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            //surplus[i]     = c[source][i];
            //surplus[source] -= c[source][i];
        }
    }
}

// Performing the delta pushesh during the up iteration
int delta_push(int **c, int **f, int *surplus, int u, int v){
    int push_flow = MIN(surplus[u], c[u][v] + f[v][u]);
    surplus[u] -= push_flow;
    surplus[v] += push_flow;
    f[v][u]   -= push_flow;

    return push_flow;
}

// Used in parallel pushesh
int push(int **c, int **f, int surplus, int u, int v){
    
    // getting the amount of flow to be pushed
    int push_flow = MIN(surplus, c[u][v] - f[u][v]);
    
    f[u][v]   += push_flow;
    return push_flow;
}

// generating push-list containing the admissible  arcs
void generate_pushlist(int vertex, int *price, int **neighbour, int **admissible, int **f, int **c){
    
    int j, index, nvertex;
        
    j = 1;
    index = 1;
    while(neighbour[vertex][j] != 0){

        nvertex = neighbour[vertex][j];

        // e+ balanced arcs condition
        if ((price[vertex] == price[nvertex] + EPSILON) && (f[vertex][nvertex] < c[vertex][nvertex])) {
            admissible[vertex][index] = nvertex;
            index ++;

        // e- balanced arcs condition
        }else if((c[vertex][nvertex] == 0) && (f[nvertex][vertex] > LB)) {
            admissible[vertex][index] = nvertex;
            index ++;
        }
        j ++;
    }

}

// up iteration performed by the master process to nullyfy the surpluses
int up_iteration(int **c, int **f, int *surplus, int source, int sink){

    int i, j = 0, sum = 0, count = 0, push_index;
    int *price        = (int *)calloc(num_vertices + 1, sizeof(int));
    
    int **admissible   = (int **)calloc(num_vertices + 1, sizeof(int));
    int **neighbour    = (int **)calloc(num_vertices + 1, sizeof(int));

    for (i = 1; i <= num_vertices; i++) {
        admissible[i] = (int *)calloc(num_vertices + 1, sizeof(int));
        neighbour[i]  = (int *)calloc(num_vertices + 1, sizeof(int));
    }

    //initializing the price of source
    price[source]   = 1;
    surplus[source] = INFINITE;

    for (i = 1; i <= num_vertices; i++) {

        //getting the neighbours
        count = 1;
        for (j = 1; j <= num_vertices; j++) {
            if (( c[i][j] != 0 ) || ( c[j][i] != 0)) {
                neighbour[i][count] = j;
                count ++;
            }
        }
    }

    // generating the pushlist for incedent arcs and discharging respective surpluses
    for (i = 2; i < num_vertices; i++) {
        push_index = 1;
        if (surplus[i] > 0) {
            generate_pushlist(i, price, neighbour, admissible, f, c);
            if (surplus[i] > 0) {
                
                // TESTING
                //print_admissible(admissible);

                //printf("Admissible edge is %d", admissible[i][push_index]);
                surplus[i] -= delta_push(c, f, surplus, i, admissible[i][push_index]);
            
                //printf("Push is performed and surplus of %d is : %d\n", i, surplus[i]);
                push_index ++;
                i = 1;
            }
        }
    }

    /*-- TESTING
    // final labels on the vertices
    printf( "\nfinal prices: \n");
    printf( "------------------------------\n");
    
    // Printing price and surpluses
    for (i = 1; i <= num_vertices; i++) {
        printf( "price[%d] : %d\n", i, price[i]);
        printf( "surplus[%d] : %d\n", i, surplus[i]);
    }
   
    printf( "------------------------------\n");
    
    --*/
    
    // maximum flow obtained at the sink
    for (i = 1; i <= num_vertices; i++) {
        if (f[i][num_vertices] > 0) {
            sum += f[i][num_vertices];
        }
    }

    printf( "maximum flow : %d\n", sum);
    printf( "------------------------------\n\n");
    
}

/*-------------------------------------------------------------*/

int main(int argc, char const *argv[])
{
    clock_t tic = clock();

    MPI_Init(NULL, NULL);
    
    int size, rank;
    
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    // Master
    //initializing the flows and the capacities
    flow       = (int **) calloc(size + 1, sizeof(int*));
    capacities = (int **) calloc(size + 1, sizeof(int*));

    for (i = 1; i <= size; i++) {
        flow[i]       = (int *) calloc(size + 1, sizeof(int));
        capacities[i] = (int *) calloc(size + 1, sizeof(int));
    }

    if (rank == 0){

        FILE *fp;
        if (argv[1] == NULL) {
            
            printf("Format is : ./para_epsilon <graphfile>\n");
            exit(0);
        
        }else {
        
            //output file
            //fo = fopen("output.txt","w");
    
            //input file
            fp = fopen(argv[1], "r");   
        }


        // getting number of vertices and edges
        fscanf(fp, "%d\t%d", &num_vertices, &num_edges);

        // number of processes should be equal to number of vertices
        if (num_vertices != size) {
            printf("Number of processes should be %d\n", num_vertices);
            printf("Exiting.......\n");
            exit(0);
        }


        // getting the info about capacities and edges
        for (i = 1; i <= num_edges; i++) {

            fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
            capacities[from][to] = capacity;
        }
        
        // printing the capacities of the matrix
        printf("\nCapacities:\n");
        print_mat(capacities);

        preflow(capacities, flow, 1);

        //--TESTING
         printf("\nAfter preflow:\n");
         print_mat(flow);
        

        //serializing the capacities 2d array for the purpose of broadcasting
        int index = 0;

        /*--ANOTHER APPROACH FOR SENDING CAPACITIES AND FLOWS
        capacities_copy = (int *)malloc(sizeof(int) * num_vertices * num_vertices);
        flow_copy       = (int *)malloc(sizeof(int) * num_vertices * num_vertices);

        for (int i = 1; i <= num_vertices; i++) {
            for (int j = 1; j <= num_vertices; j++) {
                capacities_copy[index] = capacities[i][j];
                flow_copy[index]       = flow[i][j];
                index ++;
            }
        }

        --*/

        // distributing job by master process
        for (int i = 0; i < size - 2; ++i){
            
            int temp[num_vertices];
            


            // sending outqueue to the respective processes
            //MPI_Send(outqueue, num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

            /*--ANOTHER APPROACH FOR SENDING CAPACITIES AND FLOWS
            *MPI_Send(capacities_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
            *MPI_Send(flow_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
            --*/
       }

     
    }

    // broadcasting number of vertices and edges to the other processes.
    MPI_Bcast(&num_vertices, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_edges, 1, MPI_INT, 0, MPI_COMM_WORLD);
   

    // broadcating the capacities and flows to other processes
    for (i = 1; i <= num_vertices; i++) {
        
        MPI_Bcast(&capacities[i][0], num_vertices + 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&flow[i][0], num_vertices + 1, MPI_INT, 0, MPI_COMM_WORLD);
    }


    if ((rank != 0) && (rank != num_vertices - 1)){

        // local variables required for the other processes
        int *loc_outqueue;
        loc_outqueue = flow[rank + 1] + 1;
   
        if (rank == 1){
        printf("for the process %d \n", rank);
        for (int i = 1; i <= num_vertices; i++) {
            for (int j = 1; j <= num_vertices; j++) {
                printf("%d\t", flow[i][j]);
            }
            printf("\n");
        }
        }
        


        // int capacities[num_vertices][num_vertices];
        int *send_master, *source_nodes;

        //to send to the receiving vertex
        int pushed_amount;
        
        send_master    = (int *) calloc(num_vertices, sizeof(int));
        source_nodes   = (int *) calloc(num_vertices, sizeof(int));

        int in_sum = 0, out_sum = 0, index = 0, i, j, surplus;
        int vertex_out, vertex_in;

        // pushing the flow from the process where it is possible
        source_nodes = get_mat_column(rank + 1, capacities, source_nodes);
        for (int i = 0; i < num_vertices; ++i){
           
            if (source_nodes[i] != 0){
              
                if (flow[i + 1][rank + 1] == 0) {
                   
                    printf("rank %d waiting for receive\n", rank);
                    MPI_Recv(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    //TESTING
                    //printf("I am vertex %d and from '%d' i received : %d\n", rank + 1, i + 1, pushed_amount);
                
                    // updating the flow matrix
                    flow[i + 1][rank + 1] += pushed_amount;
                }
            }
        }

        in_sum = get_mat_column_sum(rank + 1, flow);

        for (int i = 0; i < num_vertices; i++) {
            
            out_sum += loc_outqueue[i];
        }


        surplus = in_sum - out_sum;
        for (i = 1; i <= num_vertices; i++) {
            if(capacities[rank + 1][i] != 0){
                
                // TESTING 
                //printf("for vertex %d surplus is %d\n", rank + 1, surplus);

                pushed_amount = push(capacities, flow, surplus, rank + 1, i);

                // TESTING
                //printf("vertex %d sending to %d the flow %d\n", rank + 1, i, pushed_amount);

                // TESTING
                //printf("Rank %d sending to %d the flow %d", rank, i, pushed_amount);
                
                MPI_Send(&pushed_amount, 1, MPI_INT, i - 1, 0, MPI_COMM_WORLD);
                surplus -= pushed_amount;

                // TESTING
                //printf("Surplus after push %d\n", surplus);
            }
        }
           
        /*TESTING
        *printf("For process %d\n", rank);
        *printf("local flows :\n");
        *print_mat(flow);
        */

        // sending updated flows and surplus to the master
        send_master = flow[rank + 1] + 1;
        MPI_Send(&send_master[0], num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&surplus, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  
    }

    if(rank == 0){

        int *updated_flow, *surplus, temp_surplus;

        updated_flow = (int *)calloc(num_vertices, sizeof(int));
        surplus      = (int *)calloc(num_vertices + 1, sizeof(int));

        // receiving updates from childrens
        for (int i = 1; i < num_vertices - 1; ++i){
           MPI_Recv(&updated_flow[0], num_vertices, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           MPI_Recv(&temp_surplus, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           //printf("From %d the surplus received : %d\n", i, temp_surplus);
           surplus[i + 1] = temp_surplus;
           for (int j = 1; j <= num_vertices; ++j){
               
               flow[i + 1][j] = updated_flow[j - 1];

                //printf("flow from %d to %d is :%d\n", j, i+1, flow[j][i+1]);
           }
        }

        // master copy of flow
        printf("Master copy of updated flows : \n");
        print_mat(flow);

        // final updation
        up_iteration(capacities, flow, surplus, 1, num_vertices);

        // Final flows
        printf("Master copy of updated flows : \n");
        print_mat(flow);
               
    }
    MPI_Finalize();

}
