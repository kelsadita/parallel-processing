#include "para_epsilon.h"

int **alloc_2d_int(int rows, int cols) {
    int *data = (int *)calloc(rows*cols,sizeof(int));
    int **array= (int **)calloc(rows,sizeof(int*));
    int i;
    for ( i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

    return array;
}

void print_mat(int **mat) {
    int i, j;
    
    fprintf(fo,"------------------------------\n");

    for (i = 1; i <= num_vertices; i++) {
        for (j = 1; j <= num_vertices; j++)
            if (mat[i][j] > 0) {
                fprintf(fo, "from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    fprintf(fo, "\n");
    fprintf(fo,"------------------------------\n\n");
}

void print_matrix(int **mat) {
    int i, j;
    
    printf("------------------------------\n");

    for (i = 1; i <= num_vertices; i++) {
        for (j = 1; j <= num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
    printf("------------------------------\n\n");
}


//test function for printing the admissilble edges of a vertex
void print_admissible(int **mat) {
    int i, j;
    
    printf("\n\nAdmissible edges are : \n");
    printf("------------------------------\n");

    j = 1;
    for (i = 2; i <= num_vertices; i++) {
        
        if (mat[i][j] == 0) {
            break;
        }else {
            printf("%d-->%d", i, mat[i][j]);
            j ++;
        }
    }
    printf("\n------------------------------\n\n");
}


//to provide initial preflow from source to connected vertices
void preflow(int **c, int **f, int *surplus, int source){
    
    for (int i = 1; i <= num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            f[i][source] -= c[source][i];
            surplus[i]     = c[source][i];
            surplus[source] -= c[source][i];
        }
    }
}

//push operation is easy to understand
int push(int **c, int **f, int *surplus, int u, int v){
    int push_flow = MIN(surplus[u], c[u][v] - f[u][v]);
    f[u][v]   += push_flow;
    f[v][u]   -= push_flow;
    surplus[u] -= push_flow;
    surplus[v] += push_flow;

    return push_flow;
}

void incr_price(int u, int *price, int **neighbour, int **f, int **c){

        int min_height = INFINITE;
        for (int v = 1; v <= num_vertices; v++) {
            if (c[u][v] - f[u][v] > 0) {
                min_height = MIN(min_height, price[v]);
                price[u] = min_height + EPSILON;
            }
        }
}

//generating push-list containing the admissible  arcs
void generate_pushlist(int vertex, int *price, int **neighbour, int **admissible, int **f, int **c){
    
    int j, index, nvertex;
        
    j = 1;
    index = 1;
    while(neighbour[vertex][j] != 0){

        nvertex = neighbour[vertex][j];

        //e+ balanced arcs condition
        if ((price[vertex] == price[nvertex] + EPSILON) && (f[vertex][nvertex] < c[vertex][nvertex])) {
            admissible[vertex][index] = nvertex;
            index ++;
        }else if((c[vertex][nvertex] == 0) && (f[nvertex][vertex] > LB)) {
            admissible[vertex][index] = nvertex;
            index ++;
        }
        j ++;
    }

}

int up_iteration(int **c, int **f, int *surplus, int source, int sink){

    int i, j = 0, sum = 0, count = 0, push_index;
    int *price        = (int *)calloc(num_vertices + 1, sizeof(int));
    int *intermediate = (int *)calloc(num_vertices - 1, sizeof(int));
    
    
    int **admissible   = (int **)calloc(num_vertices + 1, sizeof(int));
    int **neighbour    = (int **)calloc(num_vertices + 1, sizeof(int));

    for (i = 1; i <= num_vertices; i++) {
        admissible[i] = (int *)calloc(num_vertices + 1, sizeof(int));
        neighbour[i]  = (int *)calloc(num_vertices + 1, sizeof(int));
    }

    //initializing the price of source
    price[source]   = 1;
    surplus[source] = INFINITE;

    //getting intermediate nodes for processing
    for (i = 1; i <= num_vertices; i++) {
        if (i != 1 && i != num_vertices - 1) {
            intermediate[i - 1] = i;
        }

        //getting the neighbours
        count = 1;
        for (j = 1; j <= num_vertices; j++) {
            if (( c[i][j] != 0 ) || ( c[j][i] != 0)) {
                neighbour[i][count] = j;
                count ++;
            }
        }
    }

    //processing each node for push-list with no price alteration. 
    for (i = 2; i < num_vertices; i++) {
        for (j = 3; j <= num_vertices; j++) {
            if (c[i][j] != 0) {
                push(c, f, surplus, i, j);
            }
        }
    }
    
    printf("after the first update : \n");
    print_matrix(f);

    //generating the pushlist for incedent arcs and discharging respective surpluses
    for (i = 2; i < num_vertices; i++) {
        push_index = 1;
        if (surplus[i] > 0) {
            generate_pushlist(i, price, neighbour, admissible, f, c);
            if (surplus[i] > 0) {
                //print_admissible(admissible);
                push(c, f, surplus, i, admissible[i][push_index]);
                push_index ++;
                i = 1;
            }
        }
    }
        
}

// adding function parameter f, to pass file name to master_job() method
void master_job(char *f) {
    clock_t tic = clock();   

    int **flow, **capacities, *height, i, from, to, linenumber = 1, capacity;
    
    if (f == NULL) {
        printf("Format is : ./para_epsilon <graphfile>\n");
        exit(0);
    }else {
    
    //Sending input filename to slave 1 for reading    
    MPI_Send(&f[0],9,MPI_CHAR, 1, 1, MPI_COMM_WORLD );
        //output file
        fo = fopen("output.txt","w");
    }

    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    
    flow = alloc_2d_int(num_vertices + 1,num_vertices + 1);
    capacities = alloc_2d_int(num_vertices + 1,num_vertices + 1);

    MPI_Recv(&capacities[0][0], (num_vertices + 1)*(num_vertices + 1), MPI_INT, 1, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Master has received capacities!" );
    print_matrix(capacities);

    fprintf(fo, "------------------------------\n");
    
    //Maximum flow obtained at the sink
    int sum = 0;

    MPI_Recv(&flow[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 3, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //print_matrix(flow);

    for (i = 1; i <= num_vertices; i++) {
        if (flow[i][num_vertices] > 0) {
            sum += flow[i][num_vertices];
        }
    }
    
    fprintf(fo, "Maximum flow : %d\n", sum);
    printf("Maximum flow : %d\n", sum);
    
    
    //printing the final flow in the network
    fprintf(fo, "\nFlows:\n");
    print_mat(flow);

    clock_t toc = clock();
    fprintf(fo, "Execution time : %f seconds\n", (double)(toc - tic)/(double)(1000000));
    fprintf(fo,"------------------------------\n");
}

void slave_one_job() {

    int **capacities, i, from, to, linenumber = 1, capacity;

    char *f = (char *)calloc(9, sizeof(char));
    MPI_Recv(&f[0], 9, MPI_CHAR, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    FILE *fp = fopen(f,"r");
    
    //getting number of vertices and edges
    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);
    printf("Number of vertices at slave 1 : %d\n", num_vertices);
    
    //initializing the capacities
    capacities = alloc_2d_int(num_vertices + 1,num_vertices + 1);
    

    //getting the info about capacities and edges
    for (i = 1; i < num_edges + 1; i++) {

        fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
        capacities[from][to] = capacity;
        /*
         * Once the capacities of edges leaving the source vertex are read from the input file
         * the array of those capacities can be sent to slave immidiately to calculate preflow
         */
        if ( i == num_vertices - 1) {
            MPI_Send(&num_vertices, 1, MPI_INT, 2, 1, MPI_COMM_WORLD);
            MPI_Send(&num_vertices, 1, MPI_INT, 0, 5, MPI_COMM_WORLD);
            MPI_Send(&num_vertices, 1, MPI_INT, 4, 5, MPI_COMM_WORLD);
            MPI_Send(&capacities[from][0], num_vertices + 1, MPI_INT, 2, 2, MPI_COMM_WORLD);
            MPI_Send(&capacities[from][0], num_vertices + 1, MPI_INT, 4, 2, MPI_COMM_WORLD);

        }
    }

    /*
    * TRIED FORMING A BACKUP ARRAY OF CAPACITIES TO MAKE CALLS USING SEPERATE ARRAYS
    */
    int **capacities_backup;
    capacities_backup = alloc_2d_int( num_vertices + 1, num_vertices + 1);
    for (int i = 1; i <= num_vertices; ++i)
    {
        for (int j = 1; j <= num_vertices; ++j)
        {
            capacities_backup[i][j] = capacities[i][j];
        }
    }

    /*
     * THIS IS THE MPI_SEND CALL TO SEND CAPACITIES TO SLAVE 3 WITH TAG 4, WITH NO ERRORS
     */
    MPI_Send(&capacities[0][0], (num_vertices + 1) * (num_vertices + 1) , MPI_INT, 3, 4, MPI_COMM_WORLD);
    printf("%s\n", "MPI_Send success!");
    MPI_Send(&capacities_backup[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 0, 3, MPI_COMM_WORLD);

/**********************************************************************************/
    
}

void slave_two_job() {
    int **flow;

    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Number of vertices at slave 2 : %d\n", num_vertices);
    flow   = alloc_2d_int(num_vertices + 1, num_vertices + 1);
    int *c = (int *) calloc( num_vertices + 1, sizeof(int));
    //int *surplus = (int *) calloc( num_vertices + 1, sizeof(int));

    MPI_Recv(&c[0], num_vertices + 1, MPI_INT, 1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
 
    for (int i = 1; i <= num_vertices; i++) {
        if (c[i] != 0 && i != 0) {
            flow[1][i] += c[i];
            flow[i][1] -= c[i];
            //surplus[i]  = c[i];
            //surplus[1] -= c[i];
        }
    }

    printf("Number of vertices before sending at slave 2 : %d\n", num_vertices);
    MPI_Send(&num_vertices, 1, MPI_INT, 3, 1, MPI_COMM_WORLD);
    //MPI_Send(&surplus[0], num_vertices + 1, MPI_INT, 3, 3, MPI_COMM_WORLD);
    MPI_Send(&flow[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 3, 2, MPI_COMM_WORLD);

}

void slave_three_job() {

    int **flow;
    int **capacities;

    MPI_Recv(&num_vertices, 1, MPI_INT, 2, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("Number of vertices at slave 3 : %d\n", num_vertices);

    int *surplus = (int *) calloc(num_vertices + 1, sizeof(int));
    
    capacities = alloc_2d_int( num_vertices + 1, num_vertices + 1);
    flow = alloc_2d_int(num_vertices + 1, num_vertices + 1);
    
    
    MPI_Recv(&flow[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Slave 3 has received flow");
    print_matrix(flow);
      
    /*
     * THIS IS WHERE THE MPI_Recv ERROR OCCURS :'(
     * IF U COMMENT THE FOLLOWING FOUR LINES THE ENTIRE EXECUTION HALTS EXPECTING A MESSAGE, BUT THERE ARE NO ERRORS
     */
    MPI_Recv(&capacities[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    MPI_Recv(&surplus[0], num_vertices + 1, MPI_INT, 4, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
 
    printf("%s\n", "Slave 3 has received surpluses");

    up_iteration(capacities, flow, surplus, 1, num_vertices);
    MPI_Send(&flow[0][0], (num_vertices + 1) * (num_vertices + 1), MPI_INT, 0, 1, MPI_COMM_WORLD);
    
}

void slave_four_job() {
    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Number of vertices at slave 2 : %d\n", num_vertices);
    int *c = (int *) calloc( num_vertices + 1, sizeof(int));
    int *surplus = (int *) calloc( num_vertices + 1, sizeof(int));
    MPI_Recv(&c[0], num_vertices + 1, MPI_INT, 1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    for (int i = 1; i <= num_vertices; i++) {
        if (c[i] != 0 && i != 0) {
            surplus[i]  = c[i];
            surplus[1] -= c[i];
        }
    }
    MPI_Send(&surplus[0], num_vertices + 1, MPI_INT, 3, 3, MPI_COMM_WORLD);
}


int main(int argc, char *argv[])
{
    int numprocs, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    switch(rank) {
        case 0 :
            printf("I am Master\n");
            master_job(argv[1]);
        break;

        case 1 :
            printf("I am Slave %d \n",rank);
            slave_one_job();
        break;

        case 2 :
            printf("I am Slave %d \n",rank);
            slave_two_job();
        break;

        case 3 :
            printf("I am Slave %d \n",rank);
            slave_three_job();
        break;

        case 4 :
            printf("I am Slave %d \n",rank);
            slave_four_job();
        break;
    }

    MPI_Finalize();
    return 0;
}
