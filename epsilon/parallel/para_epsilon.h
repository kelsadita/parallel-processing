#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>
#include <unistd.h>

#define MIN(X,Y) X < Y ? X : Y
#define INFINITE 10000000
#define EPSILON 1
#define LB 0

int num_vertices, num_edges;
int *inqueue, *outqueue; 
int *capacities_copy, *flow_copy;

FILE *fo;

// Matrix operations
void print_matrix(int **);

